package ar.com.coding.dojo.modelos;

public class Pino {
    private boolean derribado = false;

    public boolean isDerribado() {
        return derribado;
    }

    public void setDerribado(boolean derribado) {
        this.derribado = derribado;
    }
}
