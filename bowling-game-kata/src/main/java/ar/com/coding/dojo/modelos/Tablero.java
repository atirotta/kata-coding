package ar.com.coding.dojo.modelos;

import ar.com.coding.dojo.excepciones.TableroConNumeroNegativoException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Tablero {

    public static final int MAX_PINOS = 10;
    private int numTablero;
    private List<Pino> pinos = new ArrayList<Pino>(MAX_PINOS);

    public Tablero(int numTablero) throws TableroConNumeroNegativoException {
        this.numTablero = Optional.of(numTablero).filter(a -> a >= 0).orElseThrow(() -> new TableroConNumeroNegativoException());;
        inicializarPinos();
    }

    private void inicializarPinos() {
        for (int i = 0; i < MAX_PINOS; i++) {
            this.pinos.add(new Pino());
        }
    }

    public int getNumTablero() {
        return numTablero;
    }

    public void setNumTablero(int numTablero) {
        this.numTablero = numTablero;
    }

    public int getPinosEnPie() {
        return Math.toIntExact(pinos.stream().filter(pino -> !pino.isDerribado()).count());
    }
}
