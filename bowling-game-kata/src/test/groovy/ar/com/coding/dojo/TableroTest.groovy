package ar.com.coding.dojo

import ar.com.coding.dojo.excepciones.TableroConNumeroNegativoException
import ar.com.coding.dojo.modelos.Tablero
import spock.lang.Specification
import spock.lang.Unroll

class TableroTest extends Specification{

    @Unroll
    def "Creo un tablero #nuevoTablero con el siguiente numero: #numeroTablero"(nuevoTablero, numeroTablero){

        when: "Creo un tablero"
        Tablero tablero = nuevoTablero

        then: "La cantidad de pinos en pie es 10"
        tablero.getPinosEnPie() == 10

        and: "El numero de tablero es #numeroTablero"
        tablero.numTablero == numeroTablero

        where:
        nuevoTablero    | numeroTablero
        new Tablero(0)  | 0
        new Tablero(5)  | 5
        new Tablero(10) | 10
    }

    @Unroll
    def "No crea tablero con numero negativo #numeroTablero"(numeroTablero){

        when: "Creo un tablero"
        Tablero tablero = new Tablero(numeroTablero)

        then: "Lanza una excepcion TableroConNumeroNegativoException"
        final TableroConNumeroNegativoException exception = thrown()

        where: numeroTablero << [-1, -5, -1000]
    }

//    @Unroll
//    def "Derribo #numeroPinos en tablero #nuevoTablero"(nuevoTablero){
//
//        when: "Derribo"
//        Tablero tablero = nuevoTablero
//
//        then: "La cantidad de pinos en pie es 10"
//        tablero.pinosEnPie == 10
//
//        and: "El numero de tablero es #numeroTablero"
//        tablero.numTablero == numeroTablero
//
//        where:
//        pinosADerribar    | numeroTablero
//        new Tablero(0)  | 0
//        new Tablero(5)  | 5
//        new Tablero(10) | 10
//    }

}

